import React from "react"
//import Employees from "./Components/Employees";
// import TotalProjects from "./Components/TotalProjects";
// import './App.css';
import TotalProjects from "./Components/TotalProjects"
import TeamsIncomplete from "./Components/TeamsIncomplete"
import DelayedProjects from "./Components/DelayedProjects"
import ProjectStatus from "./Components/ProjectStatus"
import Employees from "./Components/Employees"
import EmployeeStatus from "./Components/EmployeeStatus"



function App() {
 
  return (
  <div>
  <TotalProjects />
  <TeamsIncomplete />
  <DelayedProjects />
  <ProjectStatus />
  <Employees />
  <EmployeeStatus />
  </div>
  );
}

export default App;
